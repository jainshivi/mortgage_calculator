package com.example.shivi.loancalculator;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Locale;
import java.text.DateFormatSymbols;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CalculatedFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CalculatedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */


public class CalculatedFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View rootView2;
    OutputValues vals = new OutputValues();
    Calendar c = Calendar.getInstance();

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CalculatedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CalculatedFragment newInstance(String param1, String param2) {
        CalculatedFragment fragment = new CalculatedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CalculatedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void doSomething(Values v) {

        int months = v.years * 12;
        //System.out.println(v);
        //vals.MonthlyPayment = [v.principle * v.rate * (1 + v.rate) ^ v.years]/[((1 + v.rate) ^ v.years) - 1];
        vals.MonthlyPayment = (v.principle * v.rate * Math.pow((1 + v.rate), months))/(Math.pow((1 + v.rate), months) - 1) + (v.proptax / months);
        vals.TotInterest = vals.MonthlyPayment * months - v.principle - v.proptax;
        int year = c.get(Calendar.YEAR) + v.years;
        int month = c.get(Calendar.MONTH) + months;
        int finalmonth = month % 12;
        vals.PayOffDate = getMonth(finalmonth) + " " + String.valueOf(year);
        TextView MothlyAmountVal = (TextView)rootView2.findViewById(R.id.tVMothlyAmountVal);
        TextView TotInterestVal = (TextView)rootView2.findViewById(R.id.tVTotInterestVal);
        TextView PropTaxVal = (TextView)rootView2.findViewById(R.id.tVPropTaxVal);
        TextView PayDateVal = (TextView)rootView2.findViewById(R.id.tVPayDateVal);
        //String.format("%.2f", floatValue);
        //MothlyAmountVal.setText(String.valueOf(vals.MonthlyPayment));
        MothlyAmountVal.setText(String.format("%.2f",vals.MonthlyPayment));
        TotInterestVal.setText(String.format("%.2f",vals.TotInterest));
        PropTaxVal.setText(String.format("%.2f", v.proptax));
        PayDateVal.setText(String.valueOf(vals.PayOffDate));
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            rootView2 = inflater.inflate(R.layout.fragment_calculated, container, false);
        }
        catch (InflateException e)
        {
            System.out.println(e.toString());
        }

        return rootView2;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            //throw new ClassCastException(activity.toString()
              //      + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}

package com.example.shivi.loancalculator;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.KeyEvent;

import java.util.ArrayList;
import java.util.Stack;

public class MainActivity extends Activity
        implements MainFragment.OnFragmentInteractionListener {

    final Context context = this;
    private static View view;

    public Stack<Values> history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        history = new Stack<Values>();
        initialize(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void onFragmentInteraction(){
        System.out.println("Hello");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showError(String errorMsg) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Invalid Input");
        alertDialog.setMessage(errorMsg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("values", getState());

        Parcelable [] p = new Parcelable[history.size()];
        for (int i=0; i < history.size(); i++) {
            p[i] = history.elementAt(i);
        }
        outState.putParcelableArray("history", p);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Values v = savedInstanceState.getParcelable("values");
        Parcelable [] p = savedInstanceState.getParcelableArray("history");

        history = new Stack<Values>();
        for (int i=0; i < p.length; i++) {
            history.add(i, (Values) p[i]);
        }

        initialize(v);
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void addToHistory(Values v) {
        if (getState() != v) {
            history.push(v);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // your code
            Values v = null;
            Values current = getState();
            while (!history.empty()) {
                v = history.pop();

                if (!current.equals(v)) {
                    break;
                }
                v = null;
            }

            FragmentManager fm = getFragmentManager();
            Fragment f = fm.findFragmentById(R.id.fragment);
            ((MainFragment) f).restoreState(v);

            initialize(v);

            if (current == null && v == null) {
                finish();

                return false;
            }

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public Values getState()
    {
        FragmentManager fm = getFragmentManager();
        Fragment f = fm.findFragmentById(R.id.fragment);

        try {
            return ((MainFragment) f).FindControl();
        }
        catch (NumberFormatException e) {
        }

        return null;
    }

    public void initialize(Values v)
    {
        FragmentManager fm = getFragmentManager();

        Fragment f2 = fm.findFragmentById(R.id.fragment2);
        FragmentTransaction ft = fm.beginTransaction();

        try {
            ((CalculatedFragment) f2).doSomething(v);
            ft.show(f2);
        }
        catch (Exception e) {
            ft.hide(f2);
        }

        ft.commit();
    }
}

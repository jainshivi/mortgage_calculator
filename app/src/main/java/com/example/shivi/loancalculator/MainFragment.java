package com.example.shivi.loancalculator;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.app.FragmentTransaction;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    int MonthlyPay = 0;

    private OnFragmentInteractionListener mListener;
    View rootView;
    Activity mainActivity;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    public Values FindControl() {
        //LayoutInflater inflater;
        //ViewGroup container;
        //rootView = inflater.inflate(R.layout.fragment_main, container, false);
        EditText editTextHomeVal = (EditText) rootView.findViewById(R.id.editTextHomeVal);
        EditText editTextInterestRate = (EditText) rootView.findViewById(R.id.editTextInterestRate);
        EditText editTextLoanAmount = (EditText) rootView.findViewById(R.id.editTextLoanAmount);
        EditText editTextPropertyRate = (EditText) rootView.findViewById(R.id.editTextPropertyRate);
        Spinner SpinnerYears = (Spinner) rootView.findViewById(R.id.spinnerYears);

        Values v = new Values();
        Double homevalue = 0.0;
        Double downPayment = 0.0;

        try {
            homevalue = Double.parseDouble(editTextHomeVal.getText().toString());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Invalid Home value.");
        }

        try {
            downPayment = Double.parseDouble(editTextLoanAmount.getText().toString());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Invalid down payment.");
        }

        v.homeValue = homevalue;
        v.principle = homevalue - downPayment;

        if (editTextInterestRate.getText() != null) {
            try {
                v.rate = Float.parseFloat(editTextInterestRate.getText().toString()) / 1200.0;
            } catch (NumberFormatException e) {
                throw new NumberFormatException("Please enter a valid Interest Rate.");
            }
        }

        try {
            Object o = SpinnerYears.getSelectedItem();
            v.years = Integer.parseInt(SpinnerYears.getSelectedItem().toString());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Please enter valid number of years.");
        }

        if (editTextHomeVal.getText() != null && editTextPropertyRate.getText() != null) {
            try {
                v.proptax = Double.parseDouble(editTextHomeVal.getText().toString()) * Float.parseFloat(editTextPropertyRate.getText().toString()) * v.years / 100.0;
            } catch (NumberFormatException e) {
                throw new NumberFormatException("Please enter valid property tax rate.");
            }
        }

        return v;

    }

    public void restoreState(Values v)
    {
        EditText editTextHomeVal = (EditText)rootView.findViewById(R.id.editTextHomeVal);
        EditText editTextInterestRate = (EditText)rootView.findViewById(R.id.editTextInterestRate);
        EditText editTextLoanAmount = (EditText)rootView.findViewById(R.id.editTextLoanAmount);
        EditText editTextPropertyRate = (EditText)rootView.findViewById(R.id.editTextPropertyRate);
        Spinner SpinnerYears = (Spinner)rootView.findViewById(R.id.spinnerYears);

        if (v != null) {
            editTextHomeVal.setText(String.valueOf(v.homeValue));
            editTextInterestRate.setText(String.valueOf(v.rate * 1200));
            editTextLoanAmount.setText(String.valueOf(v.homeValue - v.principle));
            editTextPropertyRate.setText(String.valueOf(v.proptax * 100 / (v.homeValue * v.years)));

            for (int i=0;i<SpinnerYears.getCount();i++){
                if (SpinnerYears.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(v.years))){
                    SpinnerYears.setSelection(i);
                    break;
                }
            }
        } else {
            editTextHomeVal.setText("");
            editTextInterestRate.setText("");
            editTextLoanAmount.setText("");
            editTextPropertyRate.setText("");
            SpinnerYears.setSelected(false);
        }
    }

    public void onButtonClicked(View view)
    {

        FragmentManager fm = getFragmentManager();

        Fragment f2 = fm.findFragmentById(R.id.fragment2);
        FragmentTransaction ft = fm.beginTransaction();

        if(view.getId() == R.id.btn_Calculate) {
            //Calculate();
            try {
                Values v = FindControl();
                ((MainActivity)mainActivity).addToHistory(v);
                ((CalculatedFragment) f2).doSomething(v);
            }
            catch (NumberFormatException e) {
                ((MainActivity)mainActivity).showError(e.getMessage());
                return;
            }

            ft.show(f2);

        }
        else
        {
            restoreState(null);
            ft.hide(f2);
        }

        ft.commit();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        try {
            rootView = inflater.inflate(R.layout.fragment_main, container, false);
        }
        catch (InflateException e) {
            System.out.println(e.toString());
        }
        Button button = (Button) rootView.findViewById(R.id.btn_Calculate);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClicked(v);
            }
        });

        Button button2 = (Button) rootView.findViewById(R.id.btn_Reset);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClicked(v);
            }
        });

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            //mListener.onFragmentInteraction(uri);
            mListener.onFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = activity;
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //public void onFragmentInteraction(Uri uri);
        public void onFragmentInteraction();
    }
}

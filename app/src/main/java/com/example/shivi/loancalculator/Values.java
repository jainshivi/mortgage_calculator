package com.example.shivi.loancalculator;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sandeep on 9/19/15.
 */
public class Values implements Parcelable {
    double homeValue;
    double principle;//is the loan amount
    double rate;
    int years;
    double proptax;

    public int describeContents() {
        return 0;
    }

    /** save object in parcel */
    public void writeToParcel(Parcel out, int flags) {
        out.writeDouble(homeValue);
        out.writeDouble(principle);
        out.writeDouble(rate);
        out.writeDouble(proptax);
        out.writeInt(years);
    }

    public static final Parcelable.Creator<Values> CREATOR
            = new Parcelable.Creator<Values>() {
        public Values createFromParcel(Parcel in) {
            return new Values(in);
        }

        public Values[] newArray(int size) {
            return new Values[size];
        }
    };

    public Values() {}

    /** recreate object from parcel */
    private Values(Parcel in) {
        homeValue = in.readDouble();
        principle = in.readDouble();
        rate = in.readDouble();
        proptax = in.readDouble();
        years = in.readInt();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Values)) {
            return false;
        }

        Values that = (Values) other;

        // Custom equality check here.
        return this.homeValue == that.homeValue &&
                this.principle == that.principle &&
                this.rate == that.rate &&
                this.years == that.years &&
                this.proptax == that.proptax;

    }
}